from django.urls import path, include
from .views import ConsultaCriptoMoedaView, ConsultaMoedaFisicaView, MoedasView
from rest_framework import routers

router = routers.DefaultRouter()
router.register('moedas', MoedasView)

urlpatterns = [
    path('', include(router.urls)),
    path('cripto-moeda/consulta/', ConsultaCriptoMoedaView.as_view(), name='consulta_criptomoeda'),
    path('moeda-fisica/consulta/', ConsultaMoedaFisicaView.as_view(), name='consulta_moedafisica'),
]