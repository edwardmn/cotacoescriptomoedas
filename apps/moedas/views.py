from django.views.generic import View
from django.shortcuts import render, HttpResponse
from django.template import loader
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CriptoMoedasForm, MoedasFisicaForm
from rest_framework import viewsets
from .serializers import MoedaSerializer
from .models import Moeda
import json
import requests


class ConsultaCriptoMoedaView(LoginRequiredMixin, View):
    template_name = 'moedas/cripto/consulta.html'
    page_template = 'moedas/cripto/resultado.html'
    cripto_api = 'https://api.coinmarketcap.com/v1/ticker/{}'

    def get(self, request, *args, **kwargs):
        form = CriptoMoedasForm()
        response = dados = None
        d = {}
        if request.is_ajax():
            form = CriptoMoedasForm(request.GET)
            form.is_valid()
            r = requests.get(self.cripto_api.format(form.cleaned_data['moeda']))
            if r.status_code == requests.codes.ok:
                try:
                    dados = json.loads(r.content)[0]
                    d['chart_data'] = [dados['percent_change_1h'], dados['percent_change_24h'], dados['percent_change_7d']]
                except:
                    dados = None
            d['html'] = loader.get_template(self.page_template).render({'dados': dados})
            response = HttpResponse(json.dumps(d), content_type='application/json')
        else:
            response = render(request, self.template_name, locals())
        return response


class ConsultaMoedaFisicaView(LoginRequiredMixin, View):
    template_name = 'moedas/fisica/consulta.html'
    page_template = 'moedas/fisica/resultado.html'
    moeda_api = 'https://economia.awesomeapi.com.br/json/{}'

    def get(self, request, *args, **kwargs):
        form = MoedasFisicaForm()
        dados = None
        if request.is_ajax():
            form = MoedasFisicaForm(request.GET)
            form.is_valid()
            r = requests.get(self.moeda_api.format(form.cleaned_data['moeda']))
            if r.status_code == requests.codes.ok:
                try:
                    dados = json.loads(r.content)[0]
                except:
                    dados = None
            self.template_name = self.page_template
        return render(request, self.template_name, locals())


class MoedasView(viewsets.ModelViewSet):
    queryset = Moeda.objects.all()
    serializer_class = MoedaSerializer
