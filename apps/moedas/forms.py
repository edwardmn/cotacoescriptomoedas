from django import forms


CRIPTO_MOEDAS_CHOICES = [
    ('bitcoin', 'Bitcoin'),
    ('ethereum', 'Ethereum'),
    ('bitcoin-cash', 'Bitcoin Cash'),
    ('stellar', 'Stellar'),
]


MOEDAS_FISICAS_CHOICE = [
    ('USD', 'Dólar Comercial'),
    ('CAD', 'Dólar Canadense'),
    ('EUR', 'Euro'),
    ('GBP', 'Libra Esterlina'),
]

class CriptoMoedasForm(forms.Form):
    moeda = forms.ChoiceField(label='Cripto Moeda', choices=CRIPTO_MOEDAS_CHOICES,
                              widget=forms.Select(attrs={'class': 'form-control'}))


class MoedasFisicaForm(forms.Form):
    moeda = forms.ChoiceField(label='Moedas Físicas', choices=MOEDAS_FISICAS_CHOICE,
                              widget=forms.Select(attrs={'class': 'form-control'}))