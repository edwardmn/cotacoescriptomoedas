from django.db import models


class Moeda(models.Model):
    class Meta:
        verbose_name = 'Moeda'
        verbose_name_plural = 'moedas'

    nome = models.CharField('Nome', max_length=100)

    def __str__(self):
        return self.nome
