from django import forms
from django.forms.utils import ErrorList
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from .models import users


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'password']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Nome'})
        self.fields['password'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Senha'})

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.username = instance.first_name
        instance.set_password(instance.password)
        if commit:
            instance.save()
        return instance


class LoginForm(forms.Form):
    _user = None
    usuario = forms.CharField(label="Usuário", widget=forms.TextInput(attrs={'tabindex': 1, 'class': 'form-control',
                                                                             'placeholder': 'Nome'}))
    senha = forms.CharField(label="Senha", max_length=20, widget=forms.PasswordInput(attrs={'tabindex': 2,
                                                                                            'class': 'form-control',
                                                                                             'placeholder': 'Senha'}))

    def clean(self):
        cleaned_data = self.cleaned_data
        if 'usuario' in cleaned_data and 'senha' in cleaned_data:
            self._user = authenticate(username=cleaned_data['usuario'], password=cleaned_data['senha'])
            if not self._user:
                self._errors["usuario"] = ErrorList(["Usuário e/ou senha inválido."])
        return cleaned_data

    @property
    def user(self):
        return self._user
