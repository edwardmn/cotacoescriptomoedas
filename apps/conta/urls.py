from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from .views import UsuarioCreate, LoginView, LogoutView, ListaUsuario


router = routers.DefaultRouter()
router.register('usuarios', ListaUsuario)


urlpatterns = [
    path('', include(router.urls)),
    path('criar/', UsuarioCreate.as_view(), name='usuario_create'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
