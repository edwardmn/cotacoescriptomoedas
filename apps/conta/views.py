from django.contrib.auth.models import User
from django.views.generic import CreateView, View
from django.urls.base import reverse_lazy, reverse
from django.shortcuts import HttpResponseRedirect, render
from django.contrib.auth import logout, login
from .forms import UserForm, LoginForm
from rest_framework import viewsets
from .serializers import UsuarioSerializer
from .models import User


class UsuarioCreate(CreateView):
    model = User
    form_class = UserForm
    template_name = 'conta/usuario_form.html'

    def get_success_url(self):
        return reverse('index')


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('login'))


class LoginView(View):
    def get(self, request, *args, **kwargs):
        form = LoginForm()
        #habilita teste de cookie
        request.session.set_test_cookie()
        return render(request, 'conta/login.html', locals())

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if not request.session.test_cookie_worked():#teste cookie
            form.add_error('usuario', 'Por favor habilite os cookies.')
        elif form.is_valid():
            #finaliza test com cookie
            request.session.delete_test_cookie()
            retorno = 'next' in request.GET and request.GET['next'] or reverse('index')
            login(request, form.user)
            request.session.set_expiry(0)
            return HttpResponseRedirect(retorno)
        return render(request, 'conta/login.html', locals())


class ListaUsuario(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UsuarioSerializer
